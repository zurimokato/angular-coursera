export class Comic{
    name:string;
    type:string;
    authorName:string;
    urlImage:string;

    constructor(name,type,authorName,urlImage){
        this.name=name;
        this.type=type;
        this.authorName=authorName;
        this.urlImage=urlImage;
    }
}