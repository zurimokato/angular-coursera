import { Component, OnInit } from '@angular/core';
import { Comic } from '../models/comic.models';

@Component({
  selector: 'app-comic-list',
  templateUrl: './comic-list.component.html',
  styleUrls: ['./comic-list.component.css']
})
export class ComicListComponent implements OnInit {

  comicList:Comic[];

  constructor() {
      this.comicList=[new Comic("Batman","Comic","Bob Kane","https://www.ecccomics.com/content/productos/6764/cubierta_detective_comics_80_an%CC%83os_de_batman_WEB.jpg"),
    new Comic("Dragon Ball","Manga","Akira Toriyama","https://vignette.wikia.nocookie.net/dragonball/images/6/6f/DB_PORTADA.jpg/revision/latest?cb=20140420052009&path-prefix=es")];
   }

  ngOnInit(): void {
  }

  guardar(name,type,author,url){
    this.comicList.push(new Comic(name,type,author,url));
    return false;
  }

}
