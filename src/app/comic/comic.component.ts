import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Comic } from '../models/comic.models';

@Component({
  selector: 'app-comic',
  templateUrl: './comic.component.html',
  styleUrls: ['./comic.component.css']
})
export class ComicComponent implements OnInit {
  @Input() comic: Comic;

  @HostBinding('attr.class') cssClass ="col-md-4";

  constructor() { }

  ngOnInit(): void {
  }

}
